UPDATE_NOTE_QUERY = """
    INSERT INTO note_{feature} (user_ip, {feature}_id, note)
    VALUES ('{ip}', {fid}, {note})
    ON CONFLICT (user_ip, {feature}_id) DO
    UPDATE SET note = {note}
    ;
"""

SELECT_NOTE_QUERY = """
    SELECT note
    FROM note_{feature}
    WHERE user_ip='{ip}' AND {feature}_id = {fid}
    ;
"""

SELECT_AVG_NOTE_QUERY = """
    SELECT AVG(note)
    FROM public_{feature}
    WHERE ogc_fid = {fid}
    ;
"""

def create_upsert_note_query(feature, ip, fid, note):
    return UPDATE_NOTE_QUERY.format(feature=feature, ip=ip, fid=fid, note=note)

def create_select_note_query(feature, ip, fid):
    return SELECT_NOTE_QUERY.format(feature=feature, ip=ip, fid=fid)

def create_select_avg_note_query(feature, fid):
    return SELECT_AVG_NOTE_QUERY.format(feature=feature, fid=fid)
