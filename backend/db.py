import time
import psycopg2

class DB:

    def __init__(self):
        self.connection = None
        while self.connection is None:
            try:
                self.connection = psycopg2.connect(user="groot",
                                                   password="groot",
                                                   host="db",
                                                   port="5432",
                                                   database="gis")
            except psycopg2.Error as e:
                time.sleep(10)
            


    def execute(self, query):
        cursor = self.connection.cursor()
        cursor.execute(query)
        self.connection.commit()
        cursor.close()

    def query(self, query):
        cursor = self.connection.cursor()
        cursor.execute(query)
        ans = cursor.fetchall()
        self.connection.commit()
        cursor.close()
        return ans;