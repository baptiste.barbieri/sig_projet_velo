from flask import Flask, request, send_file
from flask_restful import Resource, Api
from flask_cors import CORS
from db import DB
from notes import create_upsert_note_query, create_select_note_query, create_select_avg_note_query
import os
from datetime import datetime
import PIL.Image

UPLOAD_FOLDER = '/uploads'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}
BASE_HEIGHT = 400

db = DB()
app = Flask(__name__)
api = Api(app)
CORS(app)

class Note(Resource):
    
    def get(self, layer, fid):
        feature_name = layer[7:]
        query = create_select_note_query(feature_name, request.remote_addr, fid)
        rows = db.query(query)
        print(rows)
        return {'note': rows[0][0] if rows else 0}

    def put(self, layer, fid):
        note = request.json['note']
        feature_name = layer[7:]
        query = create_upsert_note_query(feature_name, request.remote_addr, fid, note)
        db.execute(query)
        return {'note' : int(db.query(create_select_avg_note_query(feature_name, fid))[0][0])}

class Issue(Resource):

    @staticmethod
    def allowedFile(filename):
        return '.' in filename and Issue.getExt(filename) in ALLOWED_EXTENSIONS

    @staticmethod
    def createDirs(layer, fid):
        path = os.path.join(UPLOAD_FOLDER, layer, fid)
        os.makedirs(path, exist_ok=True)
        return path

    @staticmethod
    def getExt(filename):
        return filename.rsplit('.', 1)[1].lower()

    @staticmethod
    def getFileName(filename):
        return '{:%Y_%m_%d_%H_%M_%S_%f}.{}'.format(datetime.now(), Issue.getExt(filename))

    @staticmethod
    def resizeImg(path):
        img = PIL.Image.open(path)
        hPercent = (BASE_HEIGHT/float(img.size[1]))
        wSize = int((float(img.size[0])*float(hPercent)))
        img = img.resize((wSize,BASE_HEIGHT), PIL.Image.ANTIALIAS)
        img.save(path)

    def get(self, layer, fid):
        path = Issue.createDirs(layer, fid)
        onlyfiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
        return { "names":
            [request.base_url+"/"+f for f in reversed(sorted(onlyfiles)[-min(5, len(onlyfiles)):])]
        }

    def post(self, layer, fid):
        file = request.files['filepond']
        if file and self.allowedFile(file.filename):
            path = os.path.join(Issue.createDirs(layer, fid), Issue.getFileName(file.filename))
            file.save(path)
            Issue.resizeImg(path)


class Image(Resource):

    def get(self, layer, fid, name):
        return send_file(os.path.join(UPLOAD_FOLDER, layer, fid, name))

api.add_resource(Note, '/note/<layer>/<fid>')
api.add_resource(Issue, '/issue/<layer>/<fid>')
api.add_resource(Image, '/issue/<layer>/<fid>/<name>')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)