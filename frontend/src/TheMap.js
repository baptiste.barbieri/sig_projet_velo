import React, { Component } from "react";

import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

import Modal from 'react-modal';
import SlidingPane from 'react-sliding-pane';
import 'react-sliding-pane/dist/react-sliding-pane.css';

import TheDetails from './TheDetails';
import SERVER_URL from './config'

import 'ol/ol.css';
import Map from 'ol/Map';
import VectorLayer  from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import * as proj from 'ol/proj';
import View from 'ol/View';
import OSM from 'ol/source/OSM';
import TileLayer from 'ol/layer/Tile';
import Overlay from 'ol/Overlay';
import {Icon, Style, Stroke} from 'ol/style';
import {bbox as bboxStrategy} from 'ol/loadingstrategy';
import GeoJSON from 'ol/format/GeoJSON';

function getIconStyle(icon){
    return (zoom) => {
        return new Style({
            image: new Icon({
                src: `img/${icon}.png`,
                anchor: [0.5, 0],
                anchorXUnits: 'fraction',
                anchorYUnits: 'fraction',
                scale: 0.08+zoom/30
            })
        });
    }
}

const HIT_TOLERANCE = 5;
const WS = "dws"
const LAYERS_NAMES = {
    'public_piste_cyclable' : {
        'title' : '$Piste cyclable',
        'content' : 'rue',
        'icon_name' : '$velo',
        'styleMaker' : (zoom) => {
            return new Style({
                stroke: new Stroke({
                    color: '#db0032',
                    width: 2
              })
            });
        },
        'useEvtCoords' : true,
    },
    'public_station_velo' : {
        'title' : '$Station Velo',
        'content' : 'nomstation',
        'icon_name': '$velo',
        'styleMaker' : getIconStyle('velo'),
        'useEvtCoords' : false,
    },
    'public_parc_relai' : {
        'title' : '$P+R Velo',
        'content' : 'nomstationvelo',
        'icon_name': '$parc_relai_velo',
        'styleMaker' : getIconStyle('parc_relai_velo'),
        'useEvtCoords' : false,
    },
}

class TheMap extends Component {

    constructor(props) {
        super(props);
        this.background = new TileLayer({
            // extent: proj.transformExtent([1.82688391208649, 47.8193550109863, 2.02002310752869, 47.9517822265625], 'EPSG:4326', 'EPSG:3857'),
            source: new OSM()
        });
        this.createLayers();
        this.view = new View({
            center: proj.fromLonLat([1.9, 47.9]),
            zoom: 12
        });
        this.map = new Map({
            target: null,
            layers: [this.background, ...this.layers],
            view: this.view
        });
        this.state = {
            isPaneOpen: false,
            isPaneOpenLeft: false,
            currentFeature: null,
            currentLayer: null
        };
        this.getGpsCoordinate();
    }

    getGpsCoordinate(){
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => { 
                let long = position.coords.longitude;
                let lat = position.coords.latitude;
                this.map.getView().setCenter(proj.transform([long, lat], 'EPSG:4326', 'EPSG:3857'));
                this.map.getView().setZoom(15);
            });
        }
    }

    createLayers(){
        this.layers = [];
        for (let n of Object.keys(LAYERS_NAMES))
            this.layers.push(this.createLayer(n));
    }

    createLayer(layerName){
        let sink = new VectorSource({
            format: new GeoJSON(),
            url: function(extent) {
                return `http://${SERVER_URL}:8600/geoserver/wfs?service=WFS&
                        version=1.1.0&
                        request=GetFeature&
                        typename=${WS}:${layerName}&
                        outputFormat=application/json&
                        srsname=EPSG:3857&
                        bbox=${extent.join(',')},EPSG:3857`.replace(/^\s+/gm, '');
            },
            strategy: bboxStrategy
        });

        return new VectorLayer({
                source : sink,
                className: layerName,
                style: (feature, resolution) => {
                    let zoom = this.map.getView().getZoom();
                    return LAYERS_NAMES[layerName].styleMaker(zoom);
                }
            });
    }

    componentDidMount() {
        this.map.setTarget("map");
        var self = this;

        this.popup = new Overlay({
          element: document.getElementById('popup')
        });
        this.map.addOverlay(this.popup);
        this.map.on('singleclick', function(e){
            self.map.forEachFeatureAtPixel(e.pixel, function(f, l){
                let elt = self.popup.getElement();
                $(elt).popover('dispose');
                self.popup.setPosition(
                    LAYERS_NAMES[l.className_].useEvtCoords
                        ? e.coordinate
                        : f.getGeometry().getCoordinates()
                    );
                $(elt).popover({
                    placement: 'top',
                    animation: false,
                    html: true,
                    title: self.getValueFromFeature(f, l, 'title'),
                    content: `<div><p>${self.getValueFromFeature(f, l, 'content')}</p>
                    <a id="paneOpener" href="#">Voir plus...</a>
                    </div>`
                });
                $(elt).popover('show');
                $("#paneOpener").click(() => {
                    self.setState({ currentFeature: f, currentLayer: l});
                    self.setState({ isPaneOpen: true });
                    $(elt).popover('dispose');
                });
                return true;
            }, {
                hitTolerance: HIT_TOLERANCE
            });
        });
        this.map.on('movestart', function(){
            let elt = self.popup.getElement();
            $(elt).popover('dispose');
        });
        Modal.setAppElement(this.el);
    }

    getValueFromFeature(f, l, v){
        let name = LAYERS_NAMES[l.className_][v];
        if (name.startsWith('$'))
            return name.substr(1);
        return f.values_[name];
    }

    renderSlidingPane() {
        let iconURL = `img/${this.getValueFromFeature(this.state.currentFeature, this.state.currentLayer, 'icon_name')}.png`
        return (
            <SlidingPane
                isOpen={ this.state.isPaneOpen }
                title={
                    <div>
                        <img style={{marginRight: 12 + 'px'}} src={iconURL} />
                        {this.getValueFromFeature(this.state.currentFeature, this.state.currentLayer, 'title')}
                    </div>
                }
                // subtitle='Optional subtitle.'
                onRequestClose={ () => {
                    // triggered on "<" on left top click or on outside click
                    this.setState({ isPaneOpen: false });
                } }>
                <TheDetails feature={this.state.currentFeature} layer={this.state.currentLayer} />
            </SlidingPane>
        );
    }

    renderLoadingSlidingPane() {
        return (
            <SlidingPane
                isOpen={ this.state.isPaneOpen }
                title="Loading ..."
                onRequestClose={ () => {
                    // triggered on "<" on left top click or on outside click
                    this.setState({ isPaneOpen: false });
                } }>
                <div>Loading...</div>
            </SlidingPane>
        );
    }

    render() {
        return (
            <div ref={ref => this.el = ref}>
                <div id="map" style={{ width: "100%", height: "100%", position:"fixed" }}>
                </div>
                <div id="popup">
                </div>
                {this.state.currentFeature ? this.renderSlidingPane() : this.renderLoadingSlidingPane()}
            </div>
        );
    }
}

export default TheMap;