import React, { Component } from "react";
import StarRatingComponent from 'react-star-rating-component';

import { FilePond, registerPlugin } from "react-filepond";
import "filepond/dist/filepond.min.css";
import FilePondPluginImageExifOrientation from "filepond-plugin-image-exif-orientation";
import FilePondPluginImagePreview from "filepond-plugin-image-preview";
import "filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css";
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import TheGallery from './TheGallery';
import SERVER_URL from './config'

const BASE_URL = `http://${SERVER_URL}:5000/`;

const FEATURE_DETAILS = {
    'public_parc_relai' : {
        'title' : 'P+R Velo',
        'fields' : {
            'Nom de station' : 'nomstationvelo',
            'Arrêt TAO le plus proche' : 'nomarrettao',
            'Commune' : 'commune',
            'Adresse' : 'adresse',
        },
        'notable' : false
    },
    'public_station_velo' : {
        'title' : 'Station Velo - oui les data sont formatées différement, merci TAO Q_Q',
        'fields' : {
            'Nom de station' : 'nomstation',
            'Commune' : 'commune',
            'Numéro' : 'numvoie',
            'Voie' : 'voie',
            'Nom de voie' : 'nomvoie',
        },
        'notable' : true
    },
    'public_piste_cyclable' : {
        'title' : 'Piste Cyclable',
        'fields' : {
            'Emplacement' : 'position',
            'Revêtement' : 'revetement',
            'Commune' : 'commune',
            'Rue' : 'rue',
            'Note' : 'observatio'
        },
        'notable' : true
    }
}

registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview, FilePondPluginFileValidateType);

class TheDetails extends Component {
    constructor(props) {
        super(props);
        if (!('feature' in props)) {
            console.log("No feature found!")
        }
        let note = 0;
        fetch(`${BASE_URL}note/${this.props.layer.className_}/${this.props.feature.values_.ogc_fid}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
            },
        })
        .then((response) => response.json())
        .then((json) => this.setState({
              rating: json.note,
              hoverRating: json.note
            }))
        .catch((error) => {
            console.error(error);
        });
        this.state = {
            globalRating: this.props.feature.values_.note,
            rating: note,
            hoverRating: note,
            files: [],
        };
    }

    onStarClick(nextValue, prevValue, name) {
        this.setState({
            rating: nextValue,
            hoverRating: nextValue
        });
        console.log(this.props.layer);
        fetch(`${BASE_URL}note/${this.props.layer.className_}/${this.props.feature.values_.ogc_fid}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                note: nextValue,
            })
        })
        .then((response) => response.json())
        .then((json) => {
            this.setState({globalRating: json.note});
            this.props.layer.getSource().refresh();
        })
        .catch((error) => {
            console.error(error);
            this.setState({
                rating: prevValue,
                hoverRating: prevValue
            });
        });
    }

    onStarHoverOut(nextValue, prevValue, name) {
        this.setState({hoverRating: this.state.rating});
    }

    onStarHover(nextValue, prevValue, name) {
        this.setState({hoverRating: nextValue});
    }

    render_details() {
        let fields = FEATURE_DETAILS[this.props.layer.className_].fields
        return (
            <div className="col-sm-6">
                <h3>Coordonnées :</h3>
            {
                Object.keys(fields).map((index) => {
                    if (this.props.feature.values_[fields[index]])
                        return (<div key={index}>{index} : {this.props.feature.values_[fields[index]]}</div>);
                })
            }
            </div>
        );
    }

    render_notes(){
        if (FEATURE_DETAILS[this.props.layer.className_].notable)
            return (
                <div className="col-sm-6">
                    <h3>Note des usagers :</h3>
                    <StarRatingComponent 
                        name="rate_noobz" 
                        starCount={10}
                        value={this.state.globalRating}
                    />
                    <br />
                    <h3>Ma note :</h3>
                    <StarRatingComponent 
                        name="rate_u" 
                        starCount={10}
                        value={this.state.hoverRating}
                        onStarClick={this.onStarClick.bind(this)}
                        onStarHover={this.onStarHover.bind(this)}
                        onStarHoverOut={this.onStarHoverOut.bind(this)}
                    />
                </div>
            );
    }

    render() {
        let issueUrl = `${BASE_URL}issue/${this.props.layer.className_}/${this.props.feature.values_.ogc_fid}`
        return (
            <div>
                <div className="row">
                    {this.render_details()}
                    {this.render_notes()}
                </div>
                <TheGallery ref={instance => (this.gallery = instance)} url={issueUrl} />
                <FilePond
                    ref={ref => (this.pond = ref)}
                    files={this.state.files}
                    allowMultiple={false}
                    maxFiles={1}
                    server={issueUrl}
                    acceptedFileTypes={['image/png', 'image/jpeg']}
                    onupdatefiles={fileItems => {
                        this.setState({
                            files: fileItems.map(fileItem => fileItem.file)
                        });
                    }}
                    onprocessfile={(error, file) => {
                            if (!error) {
                                this.gallery.fetchImages();
                                this.setState({
                                    files: []
                                });
                                toast.success('Votre image a bien été uploadée!', {
                                    position: "top-right",
                                    autoClose: 4000,
                                    hideProgressBar: false,
                                    closeOnClick: true,
                                    pauseOnHover: false,
                                    draggable: true
                                });                   
                            }
                        }
                    }
                />
                <ToastContainer
                    position="top-right"
                    autoClose={4000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange={false}
                    draggable
                    pauseOnHover={false}
                />
            </div>
        );
    }
}

export default TheDetails;
