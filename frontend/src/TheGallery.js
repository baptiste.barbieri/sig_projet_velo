import React, {Component} from 'react';
import ImageGallery from 'react-image-gallery';

const state = {
    LOADING: 0,
    NOIMG: 1,
    LOADED: 2
}

class TheGallery extends Component {

    constructor(props) {
        super(props);
        this.state = {
            images: [],
            loadState: state.LOADING
        }
        this.fetchImages();
    }

    fetchImages(){
        fetch(this.props.url, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
            },
        })
        .then((response) => response.json())
        .then((json) => {
            this.setState({
              images: this.formatItems(json.names),
              loadState: json.names.length ? state.LOADED : state.NOIMG
            });
        })
        .catch((error) => {
            console.error(error);
        });
    }

    formatItems(originals){
        let ans = [];
        for (let n of originals)
            ans.push({
                "original": n
            });
        return ans;
    }

    renderLoading(){
        return (
            <div className="image-no-gallery">Loading...</div>
        );
    }

    renderNoImg(){
        return (
            <div className="image-no-gallery">Il n'y a pas d'images ici</div>
        );
    }

    renderGallery(){
        return (
            <ImageGallery 
                items={this.state.images}
                showThumbnails={false}
            />
        );
    }

    render(){
        switch(this.state.loadState){
            case state.LOADING:
                return this.renderLoading();
            case state.NOIMG:
                return this.renderNoImg();
            case state.LOADED:
                return this.renderGallery();
        }
    }


}

export default TheGallery;