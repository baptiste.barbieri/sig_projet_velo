import React from 'react';
import logo from './logo.svg';
import './App.css';
import TheMap from "./TheMap";

function App() {
  return (
        <TheMap/>
  );
}
export default App;
