# SIG::Velos

Découvrez diverses informations utiles pour circuler en vélo à Orléans, notez les équipements et partagez des photos.

## Contexte :
Ce projet a été réalisé dans le cadre du cours de Systèmes d'Informations Géographiques de l'Université d'Orléans, par Baptiste Barbieri et Sebastien Rivault, entièrement en pair programming, sur la base de [données](https://data.orleans-metropole.fr/pages/home/) fournies par la métropole.

## Pour démarrer le projet :

```bash
$ docker-compose up
$ docker build -t populatedb init/
$ docker run -it --network sigprojetvelo_lesvelosnet populatedb
```

Le nom du network `sigprojetvelo_*` depend du nom de dossier courant.

## Contenus :

`/frontend` : un frontend react pour utiliser l'application

`/backend` : une API REST pour poster des notes et manipuler la ressource image.

`/geoserver` : les fichiers de configuration du Geoserver

`/init` : scripts d'initialisation (création et remplissage des tables en BD, configuration des couches du Geoserver)