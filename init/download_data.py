import requests

base_url = "https://data.orleans-metropole.fr/explore/dataset/%s/download/?format=geojson&timezone=Europe/Berlin"

data = {
    "parc_relai.geojson": "parcs-relais-velos-securises-tao-2018-orleans-metropole",
    "station_velo.geojson": "liste-des-stations-velo-2018-orleans-metropole",
    "piste_cyclable.geojson" : "referentielbdauao_dep_iti_cyclables",
}

for data_name, data_path in data.items():
    with open(data_name, mode='w+') as file:
        response = requests.get(base_url % data_path)
        print(response.content.decode(), file=file)
