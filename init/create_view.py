REQUEST = """
DROP VIEW IF EXISTS public_{name};
DROP TABLE IF EXISTS note_{name};

CREATE TABLE note_{name} (
    user_ip VARCHAR(15) NOT NULL,
    {name}_id integer NOT NULL,
    note integer,
    PRIMARY KEY (user_ip, {name}_id),
    CONSTRAINT {name}_fk_id FOREIGN KEY ({name}_id)
        REFERENCES {name} (ogc_fid) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
);


CREATE VIEW public_{name} AS
    SELECT {name}.*, COALESCE(AVG(n.note), 0) AS note
    FROM {name}
    LEFT JOIN note_{name} n ON {name}.ogc_fid = n.{name}_id
    GROUP BY {name}.ogc_fid;
"""

import sys

r = REQUEST.format(name = sys.argv[1][2:].split(".")[0])

print(r)

