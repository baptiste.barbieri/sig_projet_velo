#!/bin/bash

for filename in ./*.geojson; do
    ogrinfo PG:"dbname=gis user=groot password=groot port=5432 host=db" -sql "$(python3 delete_view.py $filename)";
    ogr2ogr -f "PostgreSQL" PG:"dbname=gis user=groot password=groot port=5432 host=db" "$filename" --config OGR_TRUNCATE YES
    ogrinfo PG:"dbname=gis user=groot password=groot port=5432 host=db" -sql "$(python3 create_view.py $filename)";
done
