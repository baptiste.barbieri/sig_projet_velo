import requests
from os import listdir

base_url = "http://geos:8080/geoserver/rest/%s"

auth = ('admin', 'geoserver')

headers = {
    'Content-type': 'text/xml',
}

data = '<workspace><name>dws</name></workspace>'

response_ws = requests.post(base_url % 'workspaces', 
    headers=headers, data=data, auth=auth)
# print(response_ws.status_code, response_ws.content)

data = """<dataStore>
<name>the_store</name>
<connectionParameters>
<host>db</host>
<port>5432</port>
<database>gis</database>
<user>groot</user>
<passwd>groot</passwd>
<dbtype>postgis</dbtype>
</connectionParameters>
</dataStore>"""

response_ds = requests.post(base_url % 'workspaces/dws/datastores', 
    headers=headers, data=data, auth=auth)
# print(response_ds.status_code, response_ds.content)

create_layer_request = """<featureType>
    <name>public_{name}</name>
</featureType>"""

for f in listdir("."):
    if f.endswith(".geojson"):
        name = f.split(".")[0]
        data = create_layer_request.format(name=name)
        response_layer = requests.post(base_url % 'workspaces/dws/datastores/the_store/featuretypes',
            headers=headers, data=data, auth=auth)
